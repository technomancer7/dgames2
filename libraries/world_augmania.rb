

class Worldbuilder
  def buildAugmaniaWorld
    $state.create :zone, "Debug Room"
    $state.extend "UNATCO Boat", "out", "Docks"
    $state.extend "Docks", "north", "Liberty Island"
    $state.extend "Liberty Island", "east", "Versalife Liberty Tower"
    $state.extend "Liberty Island", "west", "UNATCO Ruins: Courtyard"
    $state.extend "Liberty Island", "north", "North Docks"

    $state.extend "UNATCO Ruins: Courtyard", "in", "UNATCO Ruins: Entryway"
    $state.extend "UNATCO Ruins: Courtyard", "north", "Communication Station"
    
    $state.extend "Communication Station", "down", "Maintenance Access"
    $state.extend "Communication Station", "east", "Server Room"

    $state.extend "UNATCO Ruins: Entryway", "north", "UNATCO Ruins: Upper Hall"
    $state.extend "UNATCO Ruins: Upper Hall", "down", "UNATCO Ruins: Lower Hall"
    $state.extend "UNATCO Ruins: Upper Hall", "east", "Office 1"
    $state.extend "UNATCO Ruins: Upper Hall", "west", "Office 2"
    
    $state.extend "UNATCO Ruins: Lower Hall", "down", "UNATCO Ruins: Level 2"
    $state.extend "UNATCO Ruins: Level 2", "east", "UNATCO Ruins: Women's Bathroom"
    $state.extend "UNATCO Ruins: Level 2", "north", "UNATCO Ruins: Level 2: Medical/Tech"
    $state.extend "UNATCO Ruins: Level 2", "west", "UNATCO Ruins: Level 2: Detention/Armory"
    
    $state.extend "UNATCO Ruins: Level 2: Medical/Tech", "east", "UNATCO Ruins: Medical"
    $state.extend "UNATCO Ruins: Level 2: Medical/Tech", "west", "UNATCO Ruins: Tech"
    $state.extend "UNATCO Ruins: Level 2: Detention/Armory", "north", "UNATCO Ruins: Armory"
    $state.extend "UNATCO Ruins: Level 2: Detention/Armory", "south", "UNATCO Ruins: Detention"
    
    $state.extend "UNATCO Ruins: Lower Hall", "east", "Office 3"
    $state.extend "UNATCO Ruins: Lower Hall", "north", "UNATCO Ruins: Meeting Room"
    $state.extend "UNATCO Ruins: Meeting Room", "east", "UNATCO Ruins: Break Room"
    $state.extend "UNATCO Ruins: Lower Hall", "in", "UNATCO Ruins: Director's Office: Waiting Room"
    $state.extend "UNATCO Ruins: Director's Office: Waiting Room", "in", "UNATCO Ruins: Director's Office"
    
  end
end
