require_relative 'libdirections.rb'
            

class Worldbuilder
  def initialize
    #- Core
    @globals = {}
    @commands = {}
    @objects = {}
    @variables = {}
    @buffer = []
    @redirectInput = nil

    #- Runtime variables
    @ended = false
    @focus = 0
    @last_command = ""
    @known_events = []

    #- Initial setup
    self.defaultCommands
    self.create :actor, "player"
    self.create :zone, "start"
    self.moveto("player", "start")
  end

  attr_accessor :known_events
  
  def last_command
    @last_command
  end
  
  def focus
    @objects[@focus]
  end

  def focus=(focus)
    @focus = focus
  end

  def interrupt(&fn)
    @redirectInput = fn
  end
  
  def uninterrupt
    @redirectInput = nil
  end
  
  def ended?
    @ended
  end

  def writeln(data)
    @buffer.append data
  end

  def txt(source, msg, base = {})
    base[:source] = source
    base[:msg] = msg
    self.writeln(base)
  end

  def buffer
    @buffer
  end

  def clearBuffer
    @buffer = []
  end

  def tickAll
    for oa in @objects
      o = oa[1]
      if self.hasEvent? o, "tick"
        self.trigger o, "tick"
      end
    end
  end
  
  def read(s)
    @last_command = s
    if @redirectInput != nil
      @redirectInput.call(s)
    else
      com = s.split(" ")[0].to_sym
      ln = s.split(" ")[1..-1].join(" ")
      if @commands[com] != nil
        @commands[com].call(self, ln)
        self.tickAll
      else
        valid = []
        @commands.each do |k, v| 
          if k.to_s.downcase.start_with? com.to_s.downcase
            valid.append k
          end
        end

        if valid.length == 0
          self.txt "system", "command not found"
        elsif valid.length == 1
          @commands[valid[0]].call(self, ln)
        else
          self.txt "ambiguous", valid.to_s
        end
      end
    end
  end

  def registerGlobal(name, &fn)
    @globals[name] = fn
  end

  def global(name, input)
    @globals[name].call(self, input)
  end
  
  def registerCommand(name, &fn)
    @commands[name] = fn
  end

  def command(name, input)
    @commands[name].call(self, input)
  end
  def objectFrom(container, id)
    o = self.object container
    for item in o["contains"]
      obj = self.object item
      if obj["name"] == id or obj["id"].to_s == id.to_is
        return obj
      end
    end
  end

  def allNamesFrom(container)
    output = []
    o = self.object container
    for item in o["contains"]
      obj = self.object item
      output.append obj["name"]
    end
    return output
  end
  
  def allObjectsFrom(container)
    output = []
    o = self.object container
    for item in o["contains"]
      obj = self.object item
      output.append obj
    end
    return output
  end
  
  def objectsFrom(container, id)
    output = []
    o = self.object container
    for item in o["contains"]
      obj = self.object item
      if obj["name"].downcase == id.downcase or obj["name"].downcase.start_with?(id.downcase) or obj["id"].to_s == id.to_s
        output.append obj
      end
    end
    return output
  end
  
  def object(id)
    #puts "marshalling #{id.class} #{id}"
    if id.is_a? String or id.is_a? Numeric
      if @objects[id] != nil
        return @objects[id]
      end
      id = id.to_s
      @objects.each do |k, obj|
        if id == obj["name"] or id == obj["id"].to_s
          return obj
        end       
      end
      return nil
    else
      return id
    end
  end

  def hasEvent?(obj, event)
   # puts "obj #{obj}"
    obj = self.object obj
    #puts "event #{event}"
    #puts obj
    if obj != nil and obj["events"][event] != nil
      return true
    else
      return false
    end
  end

  def trigger(obj, event, data = {})
    obj = self.object obj
    if obj == nil
      puts "#{obj} event error, not found."; return;
    end
    
    data[:state] = self
    data[:object] = obj
    if self.hasEvent?(obj, event)
      @known_events.append event unless @known_events.include? event
      ev = obj["events"][event]
      @globals[ev].call(data)
    else
      puts "#{obj} EVENT ERROR, missing #{event}"
    end
  end
  
  def onEvent(obj, event, globalname)
    obj = self.object(obj)
    if obj != nil
      @known_events.append event unless @known_events.include? event
      obj["events"][event] = globalname
    end
  end
  
  def onEventCreate(obj, event, globalname, &fn)
    obj = self.object(obj)
    if obj != nil
      @known_events.append event unless @known_events.include? event
      @globals[globalname] = fn
      obj["events"][event] = globalname
    end
  end
  
  def create(type, name, defaultData = {})
    if type != :zone and type != :actor
      puts "Invalid type for #{name}: #{type} (not :zone or :actor symbol)"
      return
    end
   
    defaultData["type"] = type.to_s
    defaultData["name"] = name
    defaultData["contains"] = []
    if defaultData["events"] == nil
      defaultData["events"] = {}
    end
    
    defaultData["location"] = "" if type == :actor

    if type == :actor
      defaultData["health"] = 100
    end
    if type == :zone
      defaultData["visits"] = 0
      defaultData["links"] = {}
      for d in $directions
        defaultData["links"][d] = ""
      end
    end
    id = @objects.length
    defaultData["id"] = id
    @objects[id] = defaultData
    return defaultData
  end

  def spawn(name, inObj, defaultData = {})
    n = self.create(:actor, name, defaultData)
    inObj = self.object inObj
    self.moveto(n, inObj)
    return n
  end

  def moveto(obj, zone)
    obj = self.object obj      
    zone = self.object zone      

    if zone == nil
      puts "Error: invalid destination #{zone}"; return;
    end
    if obj["type"].to_sym == :zone
      puts "Error: immovable object :zone #{obj}"; return;
    end
    
    if obj["location"] != ""
      cur = self.object obj["location"]
      if cur != nil
        cur["contains"].delete obj["id"]
        self.trigger(obj, "left_zone", {zone: cur}) if self.hasEvent?(obj, 'left_zone')
      else
        puts "Invalid movement."
      end
    end
    zone["contains"].append obj["id"]
    obj["location"] = zone["id"]

    
    self.trigger(obj, "enter_zone", {zone: zone}) if self.hasEvent?(obj, 'enter_zone')
  end

  def move(obj, direction)
    d = Directions::validate direction
    obj = self.object(obj)
    if d != "" 
      z = self.object(obj["location"])
      toz = self.object(z["links"][d])
      self.moveto(obj, toz)
    end
  end

  def extend(zone, direction, newzonename)
    d = Directions::validate direction; o = Directions::invert d
    if d != "" 
      z = self.object(zone);
      if z == nil
        puts "ERROR IN EXTEND: #{zone} not found."
        return
      end
      n = self.create(:zone, newzonename); z["links"][d] = n["id"]; n["links"][o] = z["id"]
    end
  end

  def modify(o, &fn)
    obj = self.object o
    fn.call(obj)
  end
  
  def unlink(z1, dir)
    first = self.object(z1)
    dir = Directions::validate dir; o = Directions::invert dir
    second = self.object(first["links"][dir])
    first["links"][dir] = ""; second["links"][o] = ""
  end
  
  def link(z1, dir, z2)
    first = self.object(z1)
    second = self.object(z2)
    dir = Directions::validate dir; o = Directions::invert dir
    first["links"][dir] = second["id"];
    second["links"][o] = first["id"]
  end
  
  def canMove?(obj, direction)
    obj = self.object obj; zone = self.object obj["location"];
    if zone["links"][direction] != ""
      return true
    else
      return false
    end
  end
  def playerVisits(z)
    self.txt "moved", "You enter #{z['name']}...";
    p = self.focus
    if self.hasEvent? p, "moved_"+self.last_command
      self.trigger p, "moved_"+self.last_command
    end
    z = self.object z
    if z["visits"] == 0
      self.trigger(p, "first_time_zone", {zone: z}) if self.hasEvent?(p, 'first_time_zone')
      self.txt z["name"], "You see #{self.allNamesFrom(z).join(', ')}"
      z["visits"] += 1
      for d in $directions
        if z["links"][d] != ""
          out = self.object(z["links"][d])
          self.txt z["name"], "There is #{out['name']} #{d}ward"
        end
      end
      
    end
  end
  def defaultCommands
    @commands = {}

    #-- Movement commands
    self.registerCommand(:north) do |state, msg|
      if state.canMove? state.focus, "north"
        state.move 0, "north"; nl = state.object(state.focus['location']); state.playerVisits nl;
      else
        state.txt "move", "Can't go that way."
      end
    end
    self.registerCommand(:south) do |state, msg|
      if state.canMove? state.focus, "south"
        state.move 0, "south"; nl = state.object(state.focus['location']); state.playerVisits nl;
      else
        state.txt "move", "Can't go that way."
      end
    end
    self.registerCommand(:east) do |state, msg|
      if state.canMove? state.focus, "east"
        state.move 0, "east"; nl = state.object(state.focus['location']); state.playerVisits nl;
      else
        state.txt "move", "Can't go that way."
      end
    end
    self.registerCommand(:west) do |state, msg|
      if state.canMove? state.focus, "west"
        state.move 0, "west"; nl = state.object(state.focus['location']); state.playerVisits nl;
      else
        state.txt "move", "Can't go that way."
      end
    end
    self.registerCommand(:in) do |state, msg|
      if state.canMove? state.focus, "in"
        state.move 0, "in"; nl = state.object(state.focus['location']); state.playerVisits nl;
      else
        state.txt "move", "Can't go that way."
      end
    end
    self.registerCommand(:out) do |state, msg|
      if state.canMove? state.focus, "out"
        state.move 0, "out"; nl = state.object(state.focus['location']); state.playerVisits nl;
      else
        state.txt "move", "Can't go that way."
      end
    end
    self.registerCommand(:up) do |state, msg|
      if state.canMove? state.focus, "up"
        state.move 0, "up"; nl = state.object(state.focus['location']); state.playerVisits nl;
      else
        state.txt "move", "Can't go that way."
      end
    end
    self.registerCommand(:down) do |state, msg|
      if state.canMove? state.focus, "down"
        state.move 0, "down"; nl = state.object(state.focus['location']); state.playerVisits nl;
      else
        state.txt "move", "Can't go that way."
      end
    end

    
    #-- Core commands
    self.registerCommand(:speak) do |state, msg|
      pl = state.focus
      z = state.object(pl['location'])
      objs = state.objectsFrom(z, msg)
      if objs.length == 0
        state.txt "speak", "What?"
      elsif objs.length == 1
        if state.hasEvent?(objs[0], "speak")
          state.trigger(objs[0], "speak")
        else
          state.txt "touch", "You dont want to do that."
        end
      else
        state.txt "ambiguous", "You need to be more specific..."
        for item in objs
          state.txt "* ", item[:name]
        end
      end
    end
    
    self.registerCommand(:touch) do |state, msg|
      pl = state.focus
      z = state.object(pl['location'])
      objs = state.objectsFrom(z, msg)
      if objs.length == 0
        state.txt "touch", "What?"
      elsif objs.length == 1
        if state.hasEvent?(objs[0], "touch")
          state.trigger(objs[0], "touch")
        else
          state.txt "touch", "You dont want to do that."
        end
      else
        state.txt "ambiguous", "You need to be more specific..."
        for item in objs
          state.txt "* ", item['name']
        end
      end
    end

    self.registerCommand(:look) do |state, msg|
      pl = state.focus
      z = state.object(pl['location'])
      state.txt "look", "You look."
      for dir in $directions
        if z['links'][dir] != ""
          state.txt z['name'], "There is #{state.object(z['links'][dir])['name']} #{dir}ward"
        end
      end
      state.txt z["name"], "You see... #{state.allNamesFrom(z).join(', ')}"
    end

    
    #-- Inventory commands
    self.registerCommand(:inventory) do |state, msg|
      pl = state.focus
      objs = state.allNamesFrom(pl)
      state.txt "inventory", objs.to_sentence
    end
    
    self.registerCommand(:drop) do |state, msg|
      pl = state.focus
      z = state.object(pl["location"])
      objs = state.objectsFrom(pl, msg)
      if objs.length == 0
        state.txt "drop", "What?"
      elsif objs.length == 1
        if objs[0]["takeable"] == true
          state.moveto objs[0], z
          state.txt "drop", "Dropped."
          if state.hasEvent?(objs[0], "dropped")
            state.trigger(objs[0], "dropped")
          end
        else
          state.txt "drop", "Can't drop that."
        end
      else
        state.txt "ambiguous", "You need to be more specific..."
        for item in objs
          state.txt "* ", item["name"]
        end
      end
    end
      
    self.registerCommand(:use) do |state, msg|
      pl = state.focus
      z = state.object(pl["location"])
      objs = state.objectsFrom(pl, msg)
      if objs.length == 0
        state.txt "use", "What?"
      elsif objs.length == 1
        if state.hasEvent?(objs[0], "use")
          state.trigger(objs[0], "use")
        else
          state.txt "use", "Can't use that."
        end
      else
        state.txt "ambiguous", "You need to be more specific..."
        for item in objs
          state.txt "* ", item["name"]
        end
      end
    end
    
    self.registerCommand(:take) do |state, msg|
      pl = state.focus
      z = state.object(pl["location"])
      objs = state.objectsFrom(z, msg)
      if objs.length == 0
        state.txt "take", "What?"
      elsif objs.length == 1
        if objs[0]["takeable"] == true
          state.moveto objs[0], pl
          
          if state.hasEvent?(objs[0], "taken")
            state.trigger(objs[0], "taken")
          else
            state.txt "take", "Taken."
          end
        else
          state.txt "take", "Can't take that."
        end
      else
        state.txt "ambiguous", "You need to be more specific..."
        for item in objs
          state.txt "* ", item["name"]
        end
      end
    end
  end
end
