module tcm.fileplus;
import std.stdio, std.json, std.file;
import std.algorithm, std.math, std.conv;
import std.array;
import std.path;

JSONValue readDotJson(string pth){
  auto f = File(pth, "r");
  auto range = f.byLine();
  string txt;
  foreach (line; range) if (!line.empty) txt ~= line~"\n";
  return parseJSON(txt);
}

void writeDotJson(JSONValue data, string pth){ auto f = File(pth, "w"); string txt = data.toString; f.writeln(txt); }

string[] listdir(string pathname){
  return std.file.dirEntries(pathname, SpanMode.shallow).filter!(a => a.isFile).map!(a => std.path.baseName(a.name)).array;
}

string parseBytes(int n, int precision = 1){
  float step_to_greater_unit = 1024.0;
  float number_of_bytes = n.to!float;
  string unit = "bytes";

  if ((number_of_bytes / step_to_greater_unit) >= 1){
    number_of_bytes /= step_to_greater_unit;
    unit = "KB";
  }


  if ((number_of_bytes / step_to_greater_unit) >= 1){
    number_of_bytes /= step_to_greater_unit;
    unit = "MB";
  }


  if ((number_of_bytes / step_to_greater_unit) >= 1){
    number_of_bytes /= step_to_greater_unit;
    unit = "GB";
  }


  if ((number_of_bytes / step_to_greater_unit) >= 1){
    number_of_bytes /= step_to_greater_unit;
    unit = "TB";
  }

  number_of_bytes = round(number_of_bytes);

  return to!string(number_of_bytes) ~ "" ~ unit;
}

