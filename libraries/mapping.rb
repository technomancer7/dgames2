class Mapper
  def initialize(rows=10, cols=10)
    @bg          = "#"
    @spacer      = " "
    @icon        = "@"
    @autoexpand  = true
    @margins     = true
    
    @rows        = rows
    @cols        = cols
    @map         = []
    @focus       = [0, 0]
  end

  def redraw
    @map = []
    for row in 0..@rows
      @map.append [@bg]*(@cols+1)
    end
  end

  def move_to(row, col)
    self.draw @focus[0], @focus[1], @bg
    @focus = [row, col]
    self.draw @focus[0], @focus[1], @icon
  end

  def move(dir)
  end

  def draw(row, col, symbol)
    @map[row][col] = symbol
  end
  
  def to_s
    out = ""
    i = 0
    top = ""
    buf = ""
    for row in @map
      m = "" unless @margins 
      m = "#{i}#{' '*(3-i.to_s.length)}" if @margins
      out += "#{m}#{row.join(@spacer)}\n"
      
      i+=1
    end
    
    if @margins
      for l in 0..@cols
        len = @spacer.length#@bg.length-l.to_s.length
        top += "#{l}#{' '*len}"
      end

      top = "   "+top+"\n"
    end
    
    "#{buf}#{top}#{out}"
  end
  
  attr_reader :rows
  attr_reader :cols
  attr_reader :map
  
  attr_accessor :bg
  attr_accessor :icon
  attr_accessor :spacer
  attr_accessor :autoexpand
  attr_accessor :margins
end
