

class Worldbuilder
  def spawnAugmaniaItems
    $state.spawn "Trash Bag", "Docks"
    $state.spawn "Trashcan", "Docks"
    $state.spawn "UNATCO Trooper", "Docks"
    $state.spawn "Supply Box", "Docks"
    $state.spawn "Crowbar", "Docks"
    $state.spawn "Debug Door", "Docks", {"events" => {"touch" => "open_debug_door"}}
    
    $state.spawn "Pistol", $state.focus, {"takeable" => true,
                                          "clip" => 9, "ammo" => 9, "ammotype" => :pistol, "weapon" => true,
                                          "events" => {"taken" => "takengun"}}
    
    $state.spawn "Pistol", "Docks", {"takeable" => true,
                                     "clip" => 9, "ammo" => 18, "ammotype" => :pistol, "weapon" => true,
                                     "events" => {"taken" => "takengun"}}
    
    $state.spawn "Medkit", $state.focus, {"takeable" => true}
    
    $state.spawn "Riot Prod", $state.focus, {"takeable" => true,
                                             "clip" => 4, "ammo" => 12, "ammotype" => :battery, "weapon" => true,
                                             "events" => {"taken" => "takengun"}}
    
    $state.spawn "Liquor Bottle", "Docks", {"takeable" => true}

  end
end
