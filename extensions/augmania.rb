if ARGV.length > 0 then
  $LOAD_PATH.unshift ARGV[0]
  $LOAD_PATH.unshift ARGV[0]+"/libraries/"
  ARGV.delete(ARGV[0])
#else
 # $datadir = "~/.dg_data/"
end
require "libkaiser"
require "libdirections"
require "libworldbuilder"
require "libstoryteller"
require "libwearables"
require "world_augmania"
require "objects_augmania"
$state = Worldbuilder.new

#--Extensions
$state.loadWearables

#--Custom Commands
$state.registerCommand(:debug) { |state, msg| 
  $state.interrupt { |e| 
    puts "debug #{e}"
    if e == "back"
      $state.uninterrupt
    end
  }
}

#--Initial setup
$state.modify("start") { |obj|
  obj["name"] = "UNATCO Boat"
}


$state.create(:zone, "null")

#-- Create Default State
#- Map
$state.buildAugmaniaWorld
#- Objects
$state.spawnAugmaniaItems
#-- End Default State

#-- Globals
$state.onEventCreate("Trashcan", "touch", "touchtrashcan") { |s|
  s[:state].txt "trashcan", "You touch the trashcan."
}

$state.onEventCreate("player", "enter_zone", "ez") { |s|
  s[:state].txt "enter", "Event: #{s[:zone]['name']}"
}

$state.registerGlobal("takengun") { |s|
  puts "Taken gun! TODO check, find duplicates in inventory, take ammo from max and clip, give to one in inventory, discard duplicate"
}

$state.registerGlobal("open_debug_door") { |s|
  if s[:state].object("Docks")["links"]["east"] == ""
    s[:state].link "Docks", "east", "Debug Room"
    s[:state].txt "world", "The door opens eastward."
  else
    s[:state].unlink "Docks", "east"
    s[:state].txt "world", "The door closed."
  end
  
}
#-- End Globals


#-- Input Loop
if __FILE__ == $0
  while not $state.ended?
    $state.read getInput("> ")
    for b in $state.buffer
      puts "#{b[:source].blue} #{b[:msg]}"
    end
    $state.clearBuffer
  end
end


