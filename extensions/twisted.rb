if ARGV.length > 0 then
  $LOAD_PATH.unshift ARGV[0]
  $LOAD_PATH.unshift ARGV[0]+"/libraries/"
  ARGV.delete(ARGV[0])
#else
 # $datadir = "~/.dg_data/"
end
require "libkaiser"
require "libdirections"
require "libworldbuilder"
require "libstoryteller"
require "libwearables"

$state = Worldbuilder.new

#--Initial setup
$state.modify("start") { |obj|
  obj["name"] = "A dark bedroom"
}

$state.extend "A dark bedroom", "west", "A Hallway"


#-- Input Loop
if __FILE__ == $0
  while not $state.ended?
    $state.read getInput("> ")
    for b in $state.buffer
      puts "#{b[:source].blue} #{b[:msg]}"
    end
    $state.clearBuffer
  end
end


